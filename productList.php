<?php
interface ProductList
{
    public function showItems1();
    public function showItems2();
    public function showItems3();
}
?>

<?php include("productList.html") ?>
<?php //error_reporting(0); ?>
<?php include("connectionToDB.php"); ?>
<?php include "pN.php"; ?>

<script type="text/javascript" src="script2.js"></script>

<?php
class View implements ProductList
{
    public function showItems1()
    {
        $connection1 = new db();
        $connection1->conn();

        $queryForFirst = "SELECT * FROM productlist
where (sku !='0'
and name !='0'
and price!='0'
and size !='0'
)";

        echo '<br/>';

        $result = mysqli_query($connection1->conn, $queryForFirst);

        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $id = $row['id'];
                $sku = $row['SKU'];
                $name = $row['Name'];
                $price = $row['Price'];
                $size = $row['Size']; ?>
                <fieldset>
                    <input type="checkbox" name="checkbox1[]" id="chk1"
                           value="<?php echo $id; ?>"
                           onchange="checkCount(); SelectedFirst();"/>

                    <?php echo "<br/>"; ?>
                    <br/>
                    <?php echo $sku; ?>
                    <br/>
                    <?php echo $name; ?><br/>
                    <?php echo $price; ?> $<br/>
                    Size:
                    <?php echo $size; ?> MB<br/>

                </fieldset>
                <?php
            }
        }
    }

    public function showItems2()
    {
        $connection1 = new db();
        $connection1->conn();

        $queryForFirst = "SELECT * FROM productlist
where (sku !='0'
and name !='0'
and price!='0'
and height !='0'
and width !='0'
and length !='0'
)";

        $result = mysqli_query($connection1->conn, $queryForFirst);

        if ($result->num_rows > 0)
        {
            while ($row1 = $result->fetch_assoc())
            {
                $id = $row1['id'];
                $sku = $row1['SKU'];
                $name = $row1['Name'];
                $price = $row1['Price'];
                $height = $row1['Height'];
                $width = $row1['Width'];
                $length = $row1['Length'];
                ?>
                <fieldset>
                    <input type="checkbox" name="checkbox1[]" id="chk2"
                           value="<?php echo $id; ?>"
                           onchange="checkCount(); SelectedFirst();"/>

                    <?php echo "<br/>"; ?>
                    <br/>
                    <?php echo $sku; ?>
                    <br/>
                    <?php echo $name; ?><br/>
                    <?php echo $price; ?> $<br/>

                    Dimension:
                    <?php echo $height . 'x';
                    echo $width . 'x';
                    echo $length; ?>

                </fieldset>
                <?php
            }
        }
    }

    public function showItems3()
    {
        $connection1 = new db();
        $connection1->conn();

        $queryForFirst = "SELECT * FROM productlist
where (sku !='0'
and name !='0'
and price!='0'
and weight !='0'
)";

        $result = mysqli_query($connection1->conn, $queryForFirst);

        if ($result->num_rows > 0)
        {
            while ($row3 = $result->fetch_assoc())
            {
                $id = $row3['id'];
                $sku = $row3['SKU'];
                $name = $row3['Name'];
                $price = $row3['Price'];
                $weight = $row3['Weight'];
                ?>
                <fieldset>
                    <input type="checkbox" name="checkbox1[]" id="chk3"
                           value="<?php echo $id; ?>"
                           onchange="checkCount(); SelectedFirst();"/>

                    <?php echo "<br/>"; ?>
                    <?php echo "<br/>"; ?>

                    <?php echo $sku; ?>
                    <br/>
                    <?php echo $name; ?><br/>
                    <?php echo $price; ?> $<br/>

                    Weight:
                    <?php echo $weight; ?> KG

                </fieldset>
                <?php
            }
        }
    }
}
    ?>
    <?php
if(isset($_POST['delete']))
{
    if (isset($_POST['delete']))
    {
        if (isset($_POST['chk']))
        {
            $chk = $_POST['chk'];

            foreach ($chk as $idOf)
            {
                $query7 = mysqli_query($conn, "delete from productlist
                  WHERE id = $idOf");

                if (mysqli_query($conn, $query7) === true)
                {
                    echo "Records deleted";
                    echo "<br/>";
                    header("Refresh:0");
                }
            }
        }
    }
}
?>
<?php

$items1 = new View();
$items1->showItems1();

$items2 = new View();
$items2->showItems2();
//
$items3 = new View();
$items3->showItems3();
?>
