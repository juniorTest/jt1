function CheckIfAnyTextBoxIsEmpty()
{
    valid = true;

    if (document.getElementById('sku').value=="")
    {
        alert("please, enter the sku!");
        return false;
    }
    if (document.getElementById('name').value=="")
    {
        alert("please, enter the name!");
        return false;
    }
    if (document.getElementById('price').value=="")
    {
        alert("please, enter the price!");
        return false;
    }
}
function OnlyNumbers(event)
{
    var ch=String.fromCharCode(event.which);
    if(!(/[0-9]/.test(ch)))
    {
        event.preventDefault();
    }
}
function txtAppear(itemFromDDL)
{
    if(itemFromDDL.value == "1")
    {
        document.getElementById("labelOfFirstSize").style.display="block";
        document.getElementById("textbox1").style.display="block";
        document.getElementById("textInFirstDescription").style.display="block";
    }
    else
    {
        document.getElementById("labelOfFirstSize").style.display="none";
        document.getElementById("textbox1").style.display="none";
        document.getElementById("textInFirstDescription").style.display="none";
    }

    if(itemFromDDL.value=="2")
    {
        document.getElementById("labelOfFirstItemOfDimension").style.display="block";
        document.getElementById("TextFieldForHeight").style.display="block";
        document.getElementById("labelOfSecondItemOfDimension").style.display="block";
        document.getElementById("TextFieldForWidth").style.display="block";
        document.getElementById("labelOfThirdItemOfDimension").style.display="block";
        document.getElementById("TextFieldForLength").style.display="block";
        document.getElementById("textInSecondDescription").style.display="block";
    }
    else
    {
        document.getElementById("labelOfFirstItemOfDimension").style.display="none";
        document.getElementById("TextFieldForHeight").style.display="none";
        document.getElementById("labelOfSecondItemOfDimension").style.display="none";
        document.getElementById("TextFieldForWidth").style.display="none";
        document.getElementById("labelOfThirdItemOfDimension").style.display="none";
        document.getElementById("TextFieldForLength").style.display="none";
        document.getElementById("textInSecondDescription").style.display="none";
    }
    if(itemFromDDL.value=="3")
    {
        document.getElementById("labelOfSecondWeight").style.display="block";
        document.getElementById("textbox2").style.display="block";
        document.getElementById("textInThirdDescription").style.display="block";

    }
    else
    {
        document.getElementById("labelOfSecondWeight").style.display="none";
        document.getElementById("textbox2").style.display="none";
        document.getElementById("textInThirdDescription").style.display="none";

    }
}